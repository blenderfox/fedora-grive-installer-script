if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

LATESTVERSION=`wget -q -O- http://koji.russianfedora.ru/koji/packageinfo?packageID=288 | egrep -aoi 'grive(\.|\-|\w){1,}' | sort -r | egrep -m 1 '^*$'`
INFOPAGE=`wget -q -O- http://koji.russianfedora.ru/koji/packageinfo?packageID=288 | egrep -o --regexp="\".*\">$LATESTVERSION" | awk -vFS='"' '{print $2}'`

echo "Searching for latest version"
echo

DOWNLOADRPM=`wget -q -O- http://koji.russianfedora.ru/koji/$INFOPAGE | egrep -i --regexp="$LATESTVERSION\.i686" | awk -vFS='"' '{print $4}'`

echo "Latest version: $LATESTVERSION"
echo""

echo Installing from URL: $DOWNLOADRPM

yum -y localinstall $DOWNLOADRPM